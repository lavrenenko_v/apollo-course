const {gql} = require('apollo-server');



const typeDefs = gql`
type Query{
 "Query to get tracks array for the homepage grid"
  tracksForHome: [Track]
 
 "Query to get a specific track by its id"
 track(id:ID!):Track
 
 "Fetch a specific module, provided a module's ID"
  module(id: ID!): Module!
}

type Mutation {
 "Increment the number of views of a given track, when the track card is clicked"
incrementTrackViews(id:ID!):IncrementTrackViewsResponse!
}

type IncrementTrackViewsResponse {
    "Similar to HTTP status code, represents the status of the mutation"
    code: Int!
    "Indicates whether the mutation was successful"
    success: Boolean!
    "Human-readable message for the UI"
    message: String!
    "Newly updated track after a successful mutation"
    track: Track
}



 "A track is a group of Modules that teaches about a specific topic"
 type Track {
    id: ID!
    "A title of the a track"
    title: String!
    "A main author of the track"
    author: Author!
    "The main illustration of the track to display in track card or track page detail"
    thumbnail: String
    "The full duration of a track in seconds"
    durationInSeconds: Int
    "The approximate length of the track to complete, in seconds"
    length: Int @deprecated(reason:"Use durationInSeconds")
    "The number of modules the track contains"
    modulesCount: Int
    "The track's complete description, can be in Markdown format"
    description:String
    "The number of times a track has been viewed"
    numberOfViews:Int
    "A list of modules each track might have"
    modules:[Module!]!
 }
 type Module {
    id:ID!
    
    "The module's title"
    title:String!
    "The duration of a module in seconds"
    durationInSeconds:Int
    "The module's length in seconds"
    length: Int @deprecated(reason:"Use durationInSeconds")
    "The content of a module"
    content:String
    "A videoUrl to a specific module"
    videoUrl:String
    
 }
 
 "An author of a complete Track"
 type Author {
    id: ID!
    "Author's first and last name"
    name: String!
    "Author's profile picture url"
    photo: String
 }
`


module.exports = typeDefs;
