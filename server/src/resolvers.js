const resolvers = {
    Query:{
        // returns an array of Tracks that will be used to populate
        // the homepage grid of our web client
        tracksForHome: (_, __, {dataSources}, info) => {
           return dataSources.trackAPI.getTracksForHome();
        },
        // returns the track by its id for the Track page
        track:(_, {id}, {dataSources}) => {
            return dataSources.trackAPI.getTrack(id);
        },

        // get a single module by ID, for the module detail page
        module: (_, { id }, { dataSources }) => {
            return dataSources.trackAPI.getModule(id);
        },
    },

    Mutation:{
        // increments numberOfViews property of a specific track
        incrementTrackViews:async(_, {id}, {dataSources}) => {
            try{
            const track = await dataSources.trackAPI.incrementTrackViews(id);

            return {
                code: 200,
                success: true,
                message:`Successfully incremented numberOfViews property of a track with id ${id}`,
                track
            }
        }
        catch(err){
            return {
                code: err.extensions.response.status,
                success:false,
                message:err.extensions.response.body,
                track:null
            }
            }
        }
    },
    Track: {
        author: ({authorId}, _, {dataSources}, __) => {
           return dataSources.trackAPI.getAuthor(authorId);
        },
        modules: ({id}, _, {dataSources}, __) => {
            return dataSources.trackAPI.getModules(id);
        },
        durationInSeconds: ({length}) => length
    },
    Module:{
        durationInSeconds:({length}) => length
    }

};

module.exports = resolvers;
